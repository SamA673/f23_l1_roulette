import java.util.Scanner; 

public class Roulette {
    public static void main(String[]args){
        Scanner scan = new Scanner (System.in);  
        RouletteWheel wheel = new RouletteWheel(); 
        int balance = 1000;
        int guess;
        int bet; 
        int result; 
        int winnings; 


        System.out.println("-------- WELCOME TO ROULETTES! --------");
        System.out.println("Would you like to play?");
        String answer = scan.nextLine(); 

        if (answer.equals("yes")){
            System.out.println("Which number would you like to bet on? (0-36)");
            guess = Integer.parseInt(scan.nextLine()); 

            System.out.println("How much would you like to bet?");
            bet = Integer.parseInt(scan.nextLine()); 
            
            System.out.println("--------------------------");
            if (bet <= balance){
                wheel.spin();
                result = wheel.getValue(); 
                System.out.println("The wheel landed on... " + result);  

                if (result == guess){
                    winnings = bet * 35; 
                    System.out.println("You just won $" +  winnings + " dollars!");
                    balance += winnings; 
    
                } else {
                    System.out.println("You lost, but you could always double your bet and get it back :)");
                    balance -= bet; 
                }

            } else {

                System.out.println("You can't bet money you don't have...");
            }




        } 

        System.out.println("Your balance is " + balance); 
        System.out.println("Goodbye!");

        scan.close();
        }



    }

